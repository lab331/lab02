import { Observable, of } from "rxjs";
import { Student } from "../entity/student";

export abstract class StudentService {
    getStudent(): Observable<Student[]>{
        return of(this.students);
    }
    
    students:Student[] = [{
        'id': 1,
        'studentId': '562110507',
        'name': 'Prayuth',
        'surname': 'Tu',
        'gpa': 4.00
      }, {
        'id': 2,
        'studentId': '562110509',
        'name': 'Pu',
        'surname': 'Priya',
        'gpa': 2.12
      }, {
        'id': 3,
        'studentId': '562110514',
        'name': 'Tach',
        'surname': 'Tach',
        'gpa': 4.00
      }];
    
      averageGpa(): number {
        let sum = 0;
        for (let student of this.students) {
          sum += student.gpa;
        }
        return sum / this.students.length;
    
      }
}
