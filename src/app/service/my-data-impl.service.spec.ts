import { TestBed, inject } from '@angular/core/testing';

import { MyDataImplService } from './my-data-impl.service';

describe('MyDataImplService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [MyDataImplService]
    });
  });

  it('should be created', inject([MyDataImplService], (service: MyDataImplService) => {
    expect(service).toBeTruthy();
  }));
});
