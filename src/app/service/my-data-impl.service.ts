import { Injectable } from '@angular/core';
import { StudentService } from './student-service';

@Injectable({
  providedIn: 'root'
})
export class MyDataImplService extends StudentService {

  constructor() { 
    super();
  }
}
